extends Spatial

var rng = RandomNumberGenerator.new()

var night_screen
var win_screen
var gameover_screen

var timer

var villagers = [] 
# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("/root/Globals").play_sound("day_mode", true)
	villagers = $Villagers.get_children()
	for villager in villagers:
		villager.connect("is_dead", self, "on_villager_dead")
	var villager = villagers[rng.randi_range(0, villagers.size() - 1)]
	villager.kind = villager.Kind.WEREWOLF
	
	night_screen = $Night
	win_screen = $Win
	gameover_screen = $GameOver
	
	timer = $Timer

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func on_villager_dead(villager):
	if villager.kind == villager.Kind.VILLAGER:
		print("You killed a villager, you monster!")
		get_node("/root/Globals").play_sound("shoot_villager")
		villager.queue_free()
		villagers.remove(villagers.find(villager))
		if villagers.size() > 1:
			timer.set_wait_time(0.5)
			timer.connect("timeout", self, "show_night_screen")
			timer.start()
		else:
			get_node("/root/Globals").loose()
			
	elif villager.kind == villager.Kind.WEREWOLF:
		get_node("/root/Globals").play_sound("shoot_wolf")
		get_node("/root/Globals").win()

	
func show_night_screen():
	timer.disconnect("timeout", self, "show_night_screen")
	night_screen.visible = true
	timer.set_wait_time(0.5)
	timer.connect("timeout", self, "_on_night_screen_timeout")
	timer.start()
	get_node("/root/Globals").play_sound("night_mode", false)
func _on_night_screen_timeout():
	night_screen.visible = false
	timer.disconnect("timeout", self, "_on_night_screen_timeout")


