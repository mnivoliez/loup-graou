[![pipeline status](https://gitlab.deep-nope.me/mnivoliez/loup-graou/badges/master/pipeline.svg)](https://gitlab.deep-nope.me/mnivoliez/loup-graou/commits/master)
<iframe src="https://itch.io/embed/460618?linkback=true" width="552" height="167" frameborder="0"></iframe>

# Loup Graou

Développé par Baby (son et texte) et par Mathieu Nivoliez (Programmation et game design)

Page itchio: [https://mnivoliez.itch.io/loup-graou](https://mnivoliez.itch.io/loup-graou)

Version live à [https://mnivoliez.pages.gitlab.deep-nope.me/loup-graou](https://mnivoliez.pages.gitlab.deep-nope.me/loup-graou)

Submission pour la [gamejam Crée le jeu"](https://itch.io/jam/cree-le-jeu)
