# upload to itch.io 
export TOOLS_DIR=$PWD/tools
mkdir -p ${TOOLS_DIR}
cd ${TOOLS_DIR}
curl -sLo butler.zip "https://broth.itch.ovh/butler/linux-amd64-head/LATEST/.zip"
unzip butler.zip
cd ..

${TOOLS_DIR}/butler -V

${TOOLS_DIR}/butler push build/linux mnivoliez/loup-graou:linux-64
${TOOLS_DIR}/butler push build/windows mnivoliez/loup-graou:windows-64
${TOOLS_DIR}/butler push build/mac mnivoliez/loup-graou:mac-osx
${TOOLS_DIR}/butler push build/web mnivoliez/loup-graou:html5
