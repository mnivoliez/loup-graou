extends KinematicBody

signal is_dead(Villager)

enum Kind {
	VILLAGER,
	WEREWOLF
} 

const BASE_BULLET_BOOST = 50;

export (Kind) var kind

export (String, FILE, "*.txt") var villager_file : String
export (String, FILE, "*.txt") var wolf_file : String
export (String, FILE, "*.txt") var names_file : String

var villager_lines
var wolf_lines
var names

var rng = RandomNumberGenerator.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	villager_lines = load_file(villager_file)
	wolf_lines = load_file(wolf_file)
	names = load_file(names_file)

func load_file(file_path):
	var file = File.new()
	assert file.file_exists(file_path)
	file.open(file_path, file.READ)
	var lines = file.get_as_text().split('\n')
	return lines

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
		
	
func _physics_process(_delta):
	pass

func bullet_hit(damage, bullet_global_trans):
	var direction_vect = bullet_global_trans.basis.z.normalized() * BASE_BULLET_BOOST;
	
	move_and_collide(direction_vect * damage)
	emit_signal("is_dead", self)
	
func get_dialog():
	rng.randomize()
	if kind == Kind.VILLAGER:
		return villager_lines[rng.randi_range(0, villager_lines.size()-1)]
	elif kind == Kind.WEREWOLF:
		return wolf_lines[rng.randi_range(0, wolf_lines.size()-1)]