extends KinematicBody

var villager_in_range

var animation_manager
var weapons = {"PISTOL":null,}

const GRAVITY = -24.8
var vel = Vector3()
const MAX_SPEED = 20
const JUMP_SPEED = 18
const ACCEL = 4.5

var dir = Vector3()

const DEACCEL = 16
const MAX_SLOPE_ANGLE = 40

var camera
var rotation_helper

var MOUSE_SENSITIVITY = 0.05

var dialog_prompt
var dialog_box

var game_ended = false
const END_TIME = 3
var ended_time_elapse = 0
var globals

func _ready():
	camera = $Rotation_Helper/Camera
	rotation_helper = $Rotation_Helper

	animation_manager = $Rotation_Helper/Model/Animation_Player
	animation_manager.callback_function = funcref(self, "fire_bullet")

	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	weapons["PISTOL"] = $Rotation_Helper/Gun_Fire_Points/Pistol_Point
	
	dialog_box = $HUD/DialogBox
	dialog_prompt = $HUD/DialogBox/dialog_prompt
	
	globals = get_node("/root/Globals")

func _process(delta):
	if game_ended:
		process_end(delta)

func _physics_process(delta):
	if !game_ended:
		process_input(delta)
		process_movement(delta)

func process_input(_delta):

	# ----------------------------------
	# Walking
	dir = Vector3()
	var cam_xform = camera.get_global_transform()

	var input_movement_vector = Vector2()

	if Input.is_action_pressed("movement_forward"):
		input_movement_vector.y += 1
	if Input.is_action_pressed("movement_backward"):
		input_movement_vector.y -= 1
	if Input.is_action_pressed("movement_left"):
		input_movement_vector.x -= 1
	if Input.is_action_pressed("movement_right"):
		input_movement_vector.x += 1

	input_movement_vector = input_movement_vector.normalized()

	dir += -cam_xform.basis.z.normalized() * input_movement_vector.y
	dir += cam_xform.basis.x.normalized() * input_movement_vector.x
	# ----------------------------------

	# ----------------------------------
	# Capturing/Freeing cursor
	if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	# ----------------------------------
	
	# ----------------------------------
	# Firing the weapons
	if Input.is_action_pressed("fire"):
		animation_manager.set_animation("Pistol_fire")
	# ----------------------------------
	
	# ----------------------------------
	# Firing dialog
	if Input.is_action_just_pressed("interact") and villager_in_range != null:
		var pickup_line = villager_in_range.get_dialog()
		dialog_prompt.text = pickup_line
	# ----------------------------------

func process_movement(delta):
	dir.y = 0
	dir = dir.normalized()
	vel.y += delta * GRAVITY
	
	var hvel = vel
	hvel.y = 0
	
	var target = dir
	
	target *= MAX_SPEED
	
	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL
		
	hvel = hvel.linear_interpolate(target, accel * delta)
	vel.x = hvel.x
	vel.z = hvel.z
	vel = move_and_slide(vel, Vector3(0,1,0), 0.05, 4, deg2rad(MAX_SLOPE_ANGLE))
	
func process_end(delta):
	if ended_time_elapse < END_TIME:
		ended_time_elapse += delta
	else:
		globals.load_new_scene(globals.MAIN_MENU_PATH)
	
func create_sound(sound_name, position=null):
	globals.play_sound(sound_name, false, position)
	
	
func _input(event):
	if game_ended:
		return
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		rotation_helper.rotate_x(deg2rad(event.relative.y * MOUSE_SENSITIVITY))
		self.rotate_y(deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1))
		
		var camera_rot = rotation_helper.rotation_degrees
		camera_rot.x = clamp(camera_rot.x, -70, 70)
		rotation_helper.rotation_degrees = camera_rot

func fire_bullet():
	weapons["PISTOL"].fire_weapon()

func _on_DialogArea_body_entered(body):
	print ("Body entered: ", body)
	dialog_box.visible = true
	dialog_prompt.text = "Appuyez sur E pour parler"
	villager_in_range = body


func _on_DialogArea_body_exited(body):
	print ("Body exited: ", body)
	dialog_box.visible = false
	villager_in_range = null
